package aplicacion;
//import dominio.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Random;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;


public class Principal{
	public static void main(String args[]){
	  	  
	    System.out.println("_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _");
	    System.out.println("Bienvenido a tu juego de adivinanzas");
	    System.out.println("Cual es tu nombre?");
	    Scanner sc = new Scanner(System.in);
	    String nombre = sc.next();
		
	    boolean cond = true;
	    while (cond){
		System.out.println(nombre + " por favor indica el mayor numero del rango a adivinar:");
		try{
		    try{	
			Scanner num = new Scanner(System.in);	
			int num_max = num.nextInt();
		    		
		    		if (num_max < 2){
				cond = true;
				System.out.println("El numero debe ser mayor/igual que 2, ya que el rango parte del 0");
			}else{
				cond = false;
				Random rnd = new Random();
	
        		        int numRandom = rnd.nextInt(num_max + 1);
		                //trampa de guia
                		System.out.println(numRandom);

		                System.out.println("Empieza el juego, trata de adivinar el numero!");
		                boolean cond1 = true;
				//Contador para saber cuantos intentos le toma al jugador.
				int contador = 0;
		                while (cond1){
		             		contador += 1;
				     try{	
					Scanner num2 = new Scanner(System.in);    
					int guess = num2.nextInt();

        		                if (guess > num_max){
                        		        System.out.println("Intenta con un numero menor: (Debe estar comprendido entre[0,"+ num_max +"]");
		                        }else if(guess < 0){
		                                System.out.println("Intenta con un numero más alto: (Debe estar comprendido entre[0,"+ num_max +"]");
		                        }else if (guess > numRandom){
		                                System.out.println("Intenta con un numero menor:");
		                        }else if (guess < numRandom){
		                                System.out.println("Intenta con un numero más alto:");
		                        }else{
		                                System.out.println("¡Genial lo has adivinado!");
		                                System.out.println("Lo lograste en:" + contador + " intento(s)!");
						//ArrayList con los datos de la partida
						ArrayList <String> record = new ArrayList<>();
						record.add(nombre);
//					        record.add("El numero maximo es: " + numMax);
					        record.add("Intentos: " + contador);
						System.out.println(record);

						System.out.println("-Presiona 1 para volver a jugar. \n-Presiona cualquier numero para salir.");
						Scanner z = new Scanner(System.in);
						int op = z.nextInt();
						//PRECAUCION, ESTA LINEA ACTIVA UN BUCLE INFINITO QUE NO HE LOGRADO RESOLVER:
						if(op == 1){
							cond1 = false;
							sc.next();

							cond = true;
						}else{
							cond1 = false;
							cond = false;
					        }
				             }
					}catch(InputMismatchException e){
						System.out.println("Escriba \n >'help' si necesita ayuda \n >'exit' si desea salir \n >Presione cualquier tecla para volver. (Recuede, debe introducir un numero)");
        			                Scanner x = new Scanner(System.in);
				                String ayuda = x.next();
				                if(ayuda.equalsIgnoreCase("help")){
				                      System.out.println("Sigue las instucciones que se indican en cada paso.");
				                }else if (ayuda.equalsIgnoreCase("exit")){
				                        cond1 = false; 
							cond = false; 
				                }else{
                 				}
					}
		                        }
                		}
		                num.close();
				sc.close();
	    	   }catch(InputMismatchException e){
			    System.out.println("Escriba \n >'help' si necesita ayuda \n >'exit' si desea salir \n >Presione cualquier tecla para volver. (Recuede, debe introducir un numero)");
			    Scanner x = new Scanner(System.in);
			    String ayuda = x.next();
			    if(ayuda.equalsIgnoreCase("help")){
				    System.out.println("Sigue las instrucciones que se dan en cada caso.");
			    }else if (ayuda.equalsIgnoreCase("exit")){
				    cond = false;
		            }else{
			    }
		}
		}catch(NoSuchElementException e){
		}
				}
		
	}
}


